// =====================================================================================
// 
//       Filename:  pa1.h
// 
//    Description:  The magic square class
// 
//        Created:  02/18/2016 10:26:39 AM
//       Compiler:  g++
// 
//         Author:  Reece Van Atta (rv), rvanatta@ufl.edu
// 
// =====================================================================================

#include <cstdio>
#include <iostream>

// #####   TYPE DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   #########################
#define	MAX_SIZE 15			// Max size of the matrix

// =====================================================================================
//        Class:  MagicSquare
//  Description:  The magic square
// =====================================================================================
class MagicSquare
{
	public:
		// ====================  LIFECYCLE     =======================================
		MagicSquare (int size);                             // constructor

		// ====================  ACCESSORS     =======================================
		int get_size() const;

		// ====================  OPERATORS     =======================================
		void generate_square(int dir);
		void print_square();
		bool check_magic();

	private:
		// ====================  DATA MEMBERS  =======================================
		const int size;
		int matrix[MAX_SIZE][MAX_SIZE]; 

}; // -----  end of class MagicSquare  -----


//--------------------------------------------------------------------------------------
//       Class:  MagicSquare
//      Method:  MagicSquare
// Description:  Constructor Method
//--------------------------------------------------------------------------------------
MagicSquare::MagicSquare (int size)
:size(size)
{
	// Set all the elements of the matrix to 0
	for ( int i = 0; i < MAX_SIZE; i += 1 ) {
		for ( int j = 0; j < MAX_SIZE; j += 1 ) {
			this->matrix[i][j] = 0;
		}
	}
}

//--------------------------------------------------------------------------------------
//       Class:  MagicSquare
//      Method:  get_size
//--------------------------------------------------------------------------------------
	int
MagicSquare::get_size () const
{
	return this->size;
}		// -----  end of method MagicSquare::get_size  -----


//--------------------------------------------------------------------------------------
//       Class:  MagicSquare
//      Method:  generate_square
// Description:  Generates a Magic Square by the method of 
//  		https://en.wikipedia.org/wiki/Magic_square#Method_for_constructing_a_magic_square_of_odd_order
//--------------------------------------------------------------------------------------
	void
MagicSquare::generate_square ( int direction )
{
	signed int row; 
	const int& squareSize = this->size;

	if ( direction < 2 )                    // Start at first row if going north, bottom if otherwise
		row = 0;
	else
		row = squareSize - 1;
	signed int col = (squareSize / 2);    // Start at the middle column
	
	this->matrix[row][col] = 1;

	// Set the directions for which to generate
	signed int rowDir, colDir;
	switch ( direction ) {
		case 0:	
			rowDir = -1; colDir = 1;
			break;

		case 1:	
			rowDir = -1; colDir = -1;
			break;

		case 2:	
			rowDir = 1; colDir = -1;
			break;

		case 3:	
			rowDir = 1; colDir = 1;
			break;

		default:	
			std::cout	<< "What did you give me?" << std::endl;
			break;
	}				// -----  end switch  -----

	for ( int i = 2; i <= (squareSize * squareSize); ) {  	// Go until you use every number

		//----------------------------------------------------------------------
		//  The magic statement:
		//  this->matrix[(row + rowDir + squareSize) % squareSize][(col + colDir + squareSize) % squareSize]
		//
		//  Some explanation:
		//  The [(row + rowDir + squareSize) % squareSize] allows for modulo to work in both negative and positive,
		//  for instance, if row = 0, rowDir = -1, then summed up they would equal -1. Which -1 % 3 == -1, not the 2
		//  that I need. By adding squareSize to it, it makes it 2. This only works when row + rowDir > -squareSize.
		//  The columns work in much the same way.
		//  
		//----------------------------------------------------------------------

		if (this->matrix[(row + rowDir + squareSize) % squareSize][(col + colDir + squareSize) % squareSize] == 0) {

			row = (row + rowDir + squareSize) % squareSize;
			col = (col + colDir + squareSize) % squareSize;
			this->matrix[row][col] = i;
			i++;
		}
		else {
			row = (row - rowDir + squareSize) % squareSize;
			this->matrix[row][col] = i;
			i++;
		}
	}
}		// -----  end of method MagicSquare::generate_square  -----


//--------------------------------------------------------------------------------------
//       Class:  MagicSquare
//      Method:  print_square
// Description:  Prints out the magic square with good formatting
//--------------------------------------------------------------------------------------
	void
MagicSquare::print_square ( )
{
	int digitSize = 1;
	const int& squareSize = this->size;
	int currentSize = squareSize * squareSize;
	while ( currentSize > 10 ) {
		currentSize = currentSize / 10;
		digitSize++;
	}

	for ( int i = 0; i < squareSize ; i++ ) {
		std::cout << '\t';
		for ( int j = 0; j < squareSize ; j++ ) {
			printf ( "%*d", digitSize + 2, this->matrix[i][j] );
		}
		std::cout << std::endl;
	}
}		// -----  end of method MagicSquare::print_square  -----


//--------------------------------------------------------------------------------------
//       Class:  MagicSquare
//      Method:  check_magic
// Description:  Checks each row, column and diagonal to make sure it's magic
//--------------------------------------------------------------------------------------
	bool
MagicSquare::check_magic (  )
{
	bool isMagic = true;                    // Assume true, check for false

	int totalArray[MAX_SIZE * 2 + 2] = {};       // Keep track of all the totals
	int *ptotalArray = totalArray;
	const int& squareSize = this->size;
	
	std::cout	<< 	"Checking the sums of every row:      ";
	for ( int i = 0; i < squareSize; i += 1 ) {
		int rowSum = 0;
		for ( int j = 0; j < squareSize; j += 1 ) {
			rowSum += this->matrix[i][j];
		}
		*ptotalArray = rowSum; ptotalArray++;
		std::cout << rowSum << ' ';
	}
	std::cout << std::endl;

	std::cout	<< 	"Checking the sums of every column:   ";
	for ( int i = 0; i < squareSize; i += 1 ) {
		int colSum = 0;
		for ( int j = 0; j < squareSize; j += 1 ) {
			colSum += this->matrix[j][i];
		}
		*ptotalArray = colSum; ptotalArray++;
		std::cout << colSum << ' ';
	}
	std::cout << std::endl;

	std::cout << 	"Checking the sums of every diagonal: ";
	int diagSum = 0;
	for ( int i = 0; i < squareSize ; i++ ) {
		diagSum += this->matrix[i][i];
	}
	*ptotalArray = diagSum; ptotalArray++;
	std::cout << diagSum << ' ';
	diagSum = 0;
	for ( int i = 0; i < squareSize ; i++ ) {
		diagSum += this->matrix[i][squareSize - (i + 1)];
	}
	*ptotalArray = diagSum;
	std::cout << diagSum << ' ';
	std::cout << std::endl;

	int first = totalArray[0];	
	for ( int i = 0; i < squareSize * 2 + 2; i++ )
		if ( totalArray[i] != first ) {
			std::cout	<< "Hold on, that's not magic" << std::endl;
			isMagic = false;
		}
	std::cout << std::endl;
	return isMagic;
}		// -----  end of method MagicSquare::check_magic  -----

