// =====================================================================================
// 
//       Filename:  pa1.cpp
// 
//    Description:  Generates 3 Magic Squares
// 
//        Created:  02/04/2016 09:03:44 AM
//       Compiler:  g++
// 
//         Author:  Reece Van Atta (rv), rvanatta@ufl.edu
// 
// =====================================================================================


// #####   HEADER FILE INCLUDES   ###################################################
#include	<cstdlib>
#include 	<cstdio>
#include 	<iostream>
#include 	<string>
#include 	<cmath>
#include 	<vector>
#include 	"pa1.h"

// #####   TYPE DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   #########################
#define	MAX_SIZE 15			// Max size of the matrix
#define	NUM_SQUARES 3			// How many magic squares to generate
// Directions for generating the squares
#define	NE 0
#define	NW 1
#define	SW 2
#define	SE 3

// #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   #####################
bool checkMagic ( int (*pmatrix)[MAX_SIZE], int squareSize);
int getInput ();


//----------------------------------------------------------------------
//  Main Function
//----------------------------------------------------------------------
	int
main ( int argc, char *argv[] )
{
	int input = getInput();                  
	const int &squareSize = input;

	MagicSquare squareArray[NUM_SQUARES] = { MagicSquare(squareSize), MagicSquare(squareSize), MagicSquare(squareSize) };	
	// Generate the Squares
	int dirArray[] = {NE, NW, SW, SE};
	for ( int i = 0; i < NUM_SQUARES; i++ ) {
		squareArray[i].generate_square(dirArray[i]);
	}
	
	// Print and check them
	for ( int i = 0; i < NUM_SQUARES; i++ ) {
		std::cout	<< "Magic Square #" << i + 1 << " is:\n" << std::endl;
		squareArray[i].print_square();
		std::cout	<< std::endl;
		if ( !squareArray[i].check_magic() ) // Check if the square is actually magic
			return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}				// ----------  end of function main  ----------


// ===  FUNCTION  ======================================================================
//         Name:  checkInput
//  Description:  Gets the size for the square, and makes sure it's valid input
// =====================================================================================
	int
getInput ()
{
	std::string strInput;
	int size;

	bool good = false;
	// Make sure it's a valid number
	while ( !good ) {
		good = true;
		std::cout << "\nEnter the size of a magic square: ";
		std::cin >> strInput;
		// Check if the string is just numbers
		for ( unsigned int i = 0; i < strInput.length(); i += 1 ) {
			if ( !(strInput[i] >= '0' && strInput[i] <= '9') ) {
				good = false;
				std::cout << "Please enter a valid integer." << std::endl;
				break;
			}
		}
		if ( good ) {
			// Make it into an integer
			for ( signed int i = strInput.length() - 1; i >= 0; i -= 1 ) {
				size += pow(10, strInput.length() - 1 - i) * (strInput[i] - '0');
			}
			// Check if it's too big
			if (size > MAX_SIZE ) {
				std::cout << "Please enter a number less than or equal to " << MAX_SIZE << std::endl;
				good = false;
				size = 0;
			}
			// Make sure it's odd
			else if (size % 2 == 0) {
				std::cout << "Please enter an odd number." << std::endl;
				good = false;
				size = 0;
			}
		}
	}

	return size;                               // for now
}		// -----  end of function getInput  -----
